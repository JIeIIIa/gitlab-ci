FROM maven:3.6-jdk-11-slim as dependencies

WORKDIR /root/build

# copy the project files
COPY ./pom.xml ./pom.xml
COPY ./src/test/java/com/gmail/onishchenko/a/gitlabci/LoadDependencyDockerTest.java \
     ./src/test/java/com/gmail/onishchenko/a/gitlabci/LoadDependencyDockerTest.java

# build all dependencies for offline use
RUN mvn clean && \
    mvn dependency:resolve dependency:resolve-plugins test



FROM maven:3.6-jdk-11-slim as builder


COPY --from=dependencies / /

WORKDIR /root/build

RUN mvn clean && \
    mvn package



FROM adoptopenjdk/openjdk11:alpine-jre

RUN apk --no-cache add curl


WORKDIR /root/app/

COPY --from=builder /root/build/target/gitlabci-*.jar ./target/app.jar

EXPOSE 8191

ENTRYPOINT exec java -XX:+PrintFlagsFinal -XX:MaxRAMPercentage=85.0 -version | grep -Ei "maxheapsize|maxram" && \
           exec java -XX:MaxRAMPercentage=85.0 \
                     -XX:OnOutOfMemoryError="sleep 5 && kill %p &" \
                     -Dserver.port=8191 \
                     -jar target/app.jar
