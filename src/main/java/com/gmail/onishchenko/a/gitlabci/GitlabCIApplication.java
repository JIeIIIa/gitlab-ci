package com.gmail.onishchenko.a.gitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCIApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabCIApplication.class, args);
    }

}
